import sys
import wx
from test.hello import BasicTest, HolaLinux

try:
	if 'basic' in sys.argv:
		nombre = BasicTest
	elif 'hola_linux' in sys.argv:
		nombre = HolaLinux
	else:
		raise
except Exception, e:
	print "Nada encontrado, pasado a Basic"
	nombre = BasicTest

if __name__ == "__main__":
    app = nombre(False)
    app.MainLoop()